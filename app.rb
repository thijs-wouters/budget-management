require 'sinatra'
require 'sanatio'
require 'virtus'
require 'money'

$LOAD_PATH.unshift('app')

require 'support/form'

require 'budget/forms/show_budget_form'
require 'budget/use_cases/show_budget'
require 'budget/forms/change_budget_amount_form'
require 'budget/use_cases/change_budget_amount'
require 'budget/entity/budget_line'
require 'budget/repo/budget_line_repo'
require 'budget/entity/budget'
require 'budget/repo/budget_repo'

require 'categories/entity/category'
require 'categories/entity/no_category'
require 'categories/forms/add_category_form'
require 'categories/use_cases/add_category'
require 'categories/repo/category_repo'

require 'transactions/entity/transaction'
require 'transactions/repo/transaction_repo'
require 'transactions/use_cases/list_transactions'
require 'transactions/forms/list_transactions_form'
require 'transactions/use_cases/add_transaction'
require 'transactions/forms/add_transaction_form'
require 'transactions/use_cases/change_transaction'
require 'transactions/forms/change_transaction_form'

require 'accounts/repo/account_repo'
require 'accounts/entity/account'
require 'accounts/forms/list_accounts_form'
require 'accounts/use_cases/list_accounts'

require 'support/rom_setup'

require 'support/repo/memory/account_repo'

AccountRepo.use :memory

require 'support/rom/categories/category_repo_rom'
require 'support/repo/memory/category_repo'

CategoryRepo.register :rom, CategoryRepoROM.new(ROM.env)
CategoryRepo.use :rom

require 'support/rom/transactions/transaction_repo_rom'

TransactionRepo.register :rom, TransactionRepoROM.new(ROM.env)
TransactionRepo.use :rom

require 'support/rom/budget_lines/budget_line_repo_rom'

BudgetLineRepo.register :rom, BudgetLineRepoROM.new(ROM.env)
BudgetLineRepo.use :rom

require 'support/rom/budgets/budget_repo_rom'

BudgetRepo.register :rom, BudgetRepoROM.new(ROM.env)
BudgetRepo.use :rom

class ValidationError < StandardError
  attr_reader :errors

  def initialize(errors)
    super("The form is not valid: #{errors.inspect}")
    @errors = errors
  end
end

require 'web'
