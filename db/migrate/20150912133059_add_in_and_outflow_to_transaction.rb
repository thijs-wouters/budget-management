ROM::SQL.migration do
  change do
    alter_table 'transactions' do
      add_column :in_flow, Integer
      add_column :out_flow, Integer
      drop_column :amount
    end
  end
end
