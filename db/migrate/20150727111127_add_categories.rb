ROM::SQL.migration do
  change do
    create_table 'categories' do
      primary_key :id, String
      column :name, String
    end
  end
end
