ROM::SQL.migration do
  change do
    create_table 'budgets' do
      primary_key :id, String
      column :month, String
    end
  end
end
