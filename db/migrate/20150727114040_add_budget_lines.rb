ROM::SQL.migration do
  change do
    create_table 'budget_lines' do
      primary_key :id, String
      column :budget_id, String
      column :category_id, String
      column :amount, Integer
    end
  end
end
