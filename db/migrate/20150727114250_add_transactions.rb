ROM::SQL.migration do
  change do
    create_table 'transactions' do
      primary_key :id, String
      column :category_id, String
      column :budget_id, String
      column :amount, Integer
      column :date, Date
      column :comment, String
    end
  end
end
