require 'web/support/web_delivery_test'
require 'web/builders/category_builders'
require 'web/assertions/error_assertions'

module Web
  class TestAddTransaction < WebDeliveryTest
    include CategoryBuilders
    include ErrorAssertions

    def test_a_date_is_mandatory
      create_category('House')
      visit('/transactions')
      within('#add_transaction_form') do
        fill_in_form(category: 'House', date: '')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_error_shown 'Date must be present'
    end

    def test_at_least_one_flow_must_be_present
      create_category('House')
      visit('/transactions')
      within('#add_transaction_form') do
        fill_in_form(category: 'House', inflow: '', outflow: '')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_error_shown 'Either inflow or outflow must be larger than 0'
    end

    def test_only_one_of_the_flows_can_be_given
      create_category('House')
      visit('/transactions')
      within('#add_transaction_form') do
        fill_in_form(category: 'House', inflow: '100', outflow: '100')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_error_shown 'Not both inflow and outflow can be larger than 0'
    end

    private

    def fill_in_form(category:, inflow: 0, outflow: 100, date: Date.new)
      select(category, from: 'Category')
      fill_in('In flow', with: inflow)
      fill_in('Out flow', with: outflow)
      fill_in('Date', with: date)
    end
  end
end
