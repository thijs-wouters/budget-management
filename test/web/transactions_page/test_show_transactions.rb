require 'web/support/web_delivery_test'
require 'web/assertions/transaction_assertions'
require 'web/builders/transaction_builders'
require 'web/builders/category_builders'

class TestShowTransactions < WebDeliveryTest
  include TransactionAssertions
  include Web::TransactionBuilders
  include Web::CategoryBuilders

  def test_shows_the_date
    house = create_category('House')
    transaction = create_in_flow(house, 30, Date.new(2015, 1, 1))

    visit('/transactions')

    assert_equal 200, page.status_code
    assert_date_shown('01-01-2015', transaction.id)
  end

  def test_shows_the_category
    house = create_category('House')
    transaction = create_in_flow(house, 30, Date.new(2015, 1, 1))

    visit('/transactions')

    assert_equal 200, page.status_code
    assert_category_shown('House', transaction.id)
  end

  def test_show_uncategorized_transaction
    transaction = create_in_flow(NoCategory.new, 30, Date.new(2015, 1, 1))

    visit('/transactions')

    assert_equal 200, page.status_code
    assert_category_shown('Uncategorized', transaction.id)
  end

  def test_shows_the_in_flow
    house = create_category('House')
    transaction = create_in_flow(house, 30, Date.new(2015, 1, 1))

    visit('/transactions')

    assert_equal 200, page.status_code
    assert_in_flow_shown('30.00', transaction.id)
  end

  def test_shows_the_out_flow
    house = create_category('House')
    transaction = create_out_flow(house, 30, Date.new(2015, 1, 1))

    visit('/transactions')

    assert_equal 200, page.status_code
    assert_out_flow_shown('30.00', transaction.id)
  end

  def test_shows_comment
    house = create_category('House')
    transaction = create_in_flow(house, 30, Date.new(2015, 1, 1), 'comment')

    visit('/transactions')

    assert_equal 200, page.status_code
    assert_comment_shown('comment', transaction.id)
  end
end
