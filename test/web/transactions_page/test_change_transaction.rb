require 'web/support/web_delivery_test'
require 'web/assertions/transaction_assertions'
require 'web/assertions/error_assertions'
require 'web/builders/transaction_builders'
require 'web/builders/category_builders'

module Web
  class TestChangeTransaction < WebDeliveryTest
    include TransactionAssertions
    include ErrorAssertions
    include TransactionBuilders
    include CategoryBuilders

    def test_change_date
      transaction = create_in_flow(house, 40, Date.new(2015, 1, 1))

      visit('/transactions')
      within("#change_transaction_form_#{transaction.id}") do
        fill_in('Date', with: '2015-3-3')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_date_shown('03-03-2015', transaction.id)
    end

    def test_change_category
      create_category('Car')
      transaction = create_in_flow(house, 40, Date.new(2015, 1, 1))

      visit('/transactions')
      within("#change_transaction_form_#{transaction.id}") do
        select('Car', from: 'Category')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_category_shown('Car', transaction.id)
    end

    def test_change_in_flow
      transaction = create_in_flow(house, 40, Date.new(2015, 1, 1))

      visit('/transactions')
      within("#change_transaction_form_#{transaction.id}") do
        fill_in('In flow', with: '30')
        fill_in('Out flow', with: '0')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_in_flow_shown('30.00', transaction.id)
    end

    def test_change_out_flow
      transaction = create_in_flow(house, 40, Date.new(2015, 1, 1))

      visit('/transactions')
      within("#change_transaction_form_#{transaction.id}") do
        fill_in('In flow', with: '0')
        fill_in('Out flow', with: '30')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_out_flow_shown('30.00', transaction.id)
    end

    def test_change_comment
      transaction = create_in_flow(house, 40, Date.new(2015, 1, 1), 'first')

      visit('/transactions')
      within("#change_transaction_form_#{transaction.id}") do
        fill_in('Comment', with: 'comment')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_comment_shown('comment', transaction.id)
    end

    def test_a_date_is_mandatory
      transaction = create_in_flow(house, 40, Date.new(2015, 1, 1), 'first')

      visit('/transactions')
      within("#change_transaction_form_#{transaction.id}") do
        fill_in('Date', with: '')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_error_shown 'Date must be present'
    end

    def test_at_least_one_flow_must_be_present
      transaction = create_in_flow(house, 40, Date.new(2015, 1, 1), 'first')

      visit('/transactions')
      within("#change_transaction_form_#{transaction.id}") do
        fill_in('In flow', with: '')
        fill_in('Out flow', with: '')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_error_shown 'Either inflow or outflow must be larger than 0'
    end

    def test_only_one_of_the_flows_can_be_given
      transaction = create_in_flow(house, 40, Date.new(2015, 1, 1), 'first')

      visit('/transactions')
      within("#change_transaction_form_#{transaction.id}") do
        fill_in('In flow', with: '40')
        fill_in('Out flow', with: '50')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_error_shown 'Not both inflow and outflow can be larger than 0'
    end
  end
end
