require 'minitest_helper'

class TestMinusMoneyPresenter < Minitest::Test
  def test_class_is_success_when_less_than_zero
    presenter = MinusMoneyPresenter.new(Money.new(-1))

    assert_equal 'list-group-item-success', presenter.css_class
  end

  def test_class_is_success_when_equal_to_zero
    presenter = MinusMoneyPresenter.new(Money.new(0))

    assert_equal 'list-group-item-success', presenter.css_class
  end

  def test_class_is_success_when_greater_than_zero
    presenter = MinusMoneyPresenter.new(Money.new(1))

    assert_equal 'list-group-item-danger', presenter.css_class
  end
end
