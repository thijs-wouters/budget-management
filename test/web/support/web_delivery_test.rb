require 'minitest_helper'
require 'capybara'

class WebDeliveryTest < Minitest::Test
  include Capybara::DSL

  def setup
    super
    Capybara.app = Sinatra::Application.new
  end

  def assert_content(content)
    assert page.has_content?(content), "Cannot find '#{content}'"
  end
end
