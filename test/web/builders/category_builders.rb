module Web
  module CategoryBuilders
    def create_category(name)
      before = CategoryRepo.all
      visit('/budget/2015-01')
      fill_in('Name', with: name)
      click_on('add_category')
      (CategoryRepo.all - before).first
    end

    def house
      @house ||= create_category('House')
    end
  end
end
