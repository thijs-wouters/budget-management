module Web
  module TransactionBuilders
    def create_in_flow(category, amount, date, comment = '')
      return_created do
        visit('/transactions')
        within('#add_transaction_form') do
          fill_in_transaction(category, amount, 0, date, comment)
          click_on('Save')
        end
      end
    end

    def create_out_flow(category, amount, date, comment = '')
      return_created do
        visit('/transactions')
        within('#add_transaction_form') do
          fill_in_transaction(category, 0, amount, date, comment)
          click_on('Save')
        end
      end
    end

    private

    def fill_in_transaction(category, inflow, outflow, date, comment)
      select(category.name, from: 'Category')
      fill_in('Date', with: date)
      fill_in('In flow', with: inflow)
      fill_in('Out flow', with: outflow)
      fill_in('Comment', with: comment)
    end

    def return_created
      before = TransactionRepo.all
      yield
      (TransactionRepo.all - before).first
    end
  end
end
