require 'web/support/web_delivery_test'
require 'web/builders/category_builders'
require 'web/assertions/budget_assertions'

module Web
  class TestShowBudget < WebDeliveryTest
    include CategoryBuilders
    include BudgetAssertions

    def test_budgetted_amount_is_shown
      create_budget_line(house, 50, '2015-01')
      create_budget_line(house, 60, '2015-02')

      visit('/budget/2015-01')

      assert_amount_shown('50.00', house.id)
    end

    def test_category_name_is_shown
      category = create_category('Test')

      visit('/budget/2015-01')

      assert_category_name_shown('Test', category.id)
    end

    def test_amount_remaining_is_shown
      create_budget_line(house, 50, '2015-01')

      visit('/budget/2015-01')

      assert_equal 200, page.status_code
      within('#amount_to_budget') do
        assert_content 'Amount to budget'
        assert_equal '-50.00', find('.amount').text
      end
    end

    def test_overspent_is_shown
      create_budget_line(house, 50, '2015-01')
      create_out_flow(category: house, amount: 200, date: Date.new(2015, 1, 1))

      visit('/budget/2015-01')

      assert_equal 200, page.status_code
      within('#overspent') do
        assert_content 'Overspent'
        assert_equal '150.00', find('.amount').text
      end
    end

    def test_uncategorized_amount_is_shown
      create_out_flow(category: nil, amount: 200, date: Date.new(2015, 1, 1))

      visit('/budget/2015-01')

      assert_equal 200, page.status_code
      within('#uncategorized_amount') do
        assert_content 'Uncategorized amount'
        assert_equal '200.00', find('.amount').text
      end
    end

    def test_left_over_from_previous_month_is_shown
      create_in_flow(category: nil, amount: 300, date: Date.new(2015, 1, 1))
      create_budget_line(house, 200, '2015-02')

      visit('/budget/2015-02')

      assert_equal 200, page.status_code
      within('#remaining_last_month') do
        assert_content 'Remaining last month'
        assert_equal '300.00', find('.amount').text
      end
    end

    def test_income_is_shown
      create_budget_line(house, 200, '2015-01')
      create_in_flow(category: nil, amount: 300, date: Date.new(2015, 1, 1))

      visit('/budget/2015-01')

      assert_equal 200, page.status_code
      within('#income') do
        assert_content 'Income'
        assert_equal '300.00', find('.amount').text
      end
    end

    def test_balance_is_shown
      create_budget_line(house, 200, '2015-01')
      create_out_flow(category: house, amount: 50, date: Date.new(2015, 1, 1))

      visit('/budget/2015-01')

      assert_equal 200, page.status_code
      within("##{house.id}") do
        assert_equal '150.00', find('.balance').text
      end
    end

    def test_outflow_is_shown
      create_budget_line(house, 240, '2015-01')
      create_out_flow(category: house, amount: 50, date: Date.new(2015, 1, 1))

      visit('/budget/2015-01')

      assert_equal 200, page.status_code
      within("##{house.id}") do
        assert_equal '50.00', find('.outflow').text
      end
    end
  end
end
