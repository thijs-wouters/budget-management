require 'web/support/web_delivery_test'

module Web
  class TestAddCategory < WebDeliveryTest
    def test_add_category
      visit('/budget/2015-01')
      fill_in('Name', with: 'test')
      click_on('add_category')

      assert_equal 200, page.status_code
    end

    def test_name_is_mandatory
      visit('/budget/2015-01')
      fill_in('Name', with: '')
      click_on('add_category')

      within '#errors' do
        assert_content 'Name must be present'
      end
    end
  end
end
