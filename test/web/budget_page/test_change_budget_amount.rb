require 'web/support/web_delivery_test'
require 'web/assertions/budget_assertions'
require 'web/assertions/error_assertions'
require 'web/builders/category_builders'

module Web
  class TestChangeBudgetAmount < WebDeliveryTest
    include BudgetAssertions
    include ErrorAssertions
    include CategoryBuilders

    def test_change_amount
      create_budget_line(house, 50, '2015-01')

      visit('/budget/2015-01')
      fill_in('House', with: 70)
      click_on("change_category_#{house.id}")

      assert_equal 200, page.status_code
      assert_amount_shown('70.00', house.id)
    end

    def test_amount_must_be_positive
      visit('/budget/2015-01')
      within("##{house.id}") do
        fill_in('amount', with: '-30')
        click_on('Save')
      end

      assert_equal 200, page.status_code
      assert_error_shown('Amount must be larger than 0')
    end
  end
end
