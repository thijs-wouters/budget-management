module TransactionAssertions
  def assert_date_shown(date, transaction_id)
    within("##{transaction_id}") do
      assert_equal date, find('[name=date]').value
    end
  end

  def assert_in_flow_shown(in_flow, transaction_id)
    within("##{transaction_id}") do
      assert_equal in_flow, find('[name=in_flow]').value
    end
  end

  def assert_out_flow_shown(out_flow, transaction_id)
    within("##{transaction_id}") do
      assert_equal out_flow, find('[name=out_flow]').value
    end
  end

  def assert_category_shown(category, transaction_id)
    within("##{transaction_id}") do
      assert_equal category, find('[name=category] option[selected]').text
    end
  end

  def assert_comment_shown(comment, transaction_id)
    within("##{transaction_id}") do
      assert_equal comment, find('[name=comment]').value
    end
  end
end
