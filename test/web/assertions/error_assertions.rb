module ErrorAssertions
  def assert_error_shown(error)
    within('#errors') do
      assert_equal error, find('li').text
    end
  end
end
