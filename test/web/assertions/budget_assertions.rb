module BudgetAssertions
  def assert_amount_shown(amount, category_id)
    within("##{category_id}") do
      assert_equal amount, find('[name=amount]').value
    end
  end

  def assert_category_name_shown(name, category_id)
    within("##{category_id}") do
      assert_equal name, find('label').text
    end
  end
end
