require 'minitest_helper'

class TestListAccounts < Minitest::Test
  def test_returns_the_id
    create_account(id: 1)

    form = ListAccountsForm.new
    result = ListAccounts.new(form).execute

    assert_equal(1, result.first.id)
  end

  def test_returns_the_name
    create_account(name: 'test')

    form = ListAccountsForm.new
    result = ListAccounts.new(form).execute

    assert_equal('test', result.first.name)
  end
end
