require 'simplecov'
SimpleCov.start

require_relative '../app'

require 'minitest'
require 'minitest/autorun'
require 'minitest/reporters'
Minitest::Reporters.use!
Minitest.backtrace_filter.add_filter(/\.rvm/)

module Minitest
  class Test
    def setup
      CategoryRepo.clear
      BudgetLineRepo.clear
      BudgetRepo.clear
      TransactionRepo.clear
      AccountRepo.clear

      assert_empty CategoryRepo
      assert_empty BudgetLineRepo
      assert_empty BudgetRepo
      assert_empty TransactionRepo
      assert_empty AccountRepo
    end

    def house
      @house ||= create_category('House')
    end

    def car
      @car ||= create_category('Car')
    end
  end
end

def refute_validates(field: nil, reason:, params: [])
  exception = assert_raises ValidationError do
    yield
  end

  refute_nil exception.errors
  assert_equal 1, exception.errors.length

  error = exception.errors[0]
  assert_equal field, error.field if field
  assert_equal reason, error.reason
  assert_equal params, error.params if params
end

def assert_validation_exception(exception, field, reason, params = [])
  refute_nil exception.errors
  assert_equal 1, exception.errors.length

  error = exception.errors[0]
  assert_equal field, error.field
  assert_equal reason, error.reason
  assert_equal params, error.params if params
end

def assert_budget(amount_to_budget, expected, budget)
  assert_equal amount_to_budget, budget.amount_to_budget

  expected.each do |category, amount|
    assert_equal amount, budget[category].amount
    assert_includes budget.categories, category
  end
end

def create_category(name)
  form = AddCategoryForm.new(name: name)
  AddCategory.new(form).execute
end

def create_budget_line(category, amount, month)
  form = ChangeBudgetAmountForm.new(
    category: category,
    amount: amount,
    month: month)
  ChangeBudgetAmount.new(form).execute
end

def create_account(id: 1, name: 'account')
  Account.new(id: id, name: name).save
end

def create_in_flow(category: house, amount: 20, date: Date.new, comment: '')
  form = AddTransactionForm.new(
    category: category,
    in_flow: amount,
    date: date,
    comment: comment)
  AddTransaction.new(form).execute
end

def create_out_flow(category: house, amount: 20, date: Date.new, comment: '')
  form = AddTransactionForm.new(
    category: category,
    out_flow: amount,
    date: date,
    comment: comment)
  AddTransaction.new(form).execute
end
