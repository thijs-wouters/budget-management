require 'minitest_helper'

class TestAddCategory < Minitest::Test
  def test_it_adds_the_category
    form = AddCategoryForm.new(name: 'New Category')
    use_case = AddCategory.new(form)

    use_case.execute

    refute_empty CategoryRepo
    assert_equal 1, CategoryRepo.count

    new_category = CategoryRepo.first
    refute_nil new_category.id
    assert_equal 'New Category', new_category.name
  end

  def test_the_name_must_be_given
    form = AddCategoryForm.new(name: '')
    use_case = AddCategory.new(form)

    e = assert_raises ValidationError do
      use_case.execute
    end

    assert_empty CategoryRepo
    assert_validation_exception(e, :name, :not_present)
  end
end
