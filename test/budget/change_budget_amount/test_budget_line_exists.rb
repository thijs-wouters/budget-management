require 'minitest_helper'

class TestBudgetLineExists < Minitest::Test
  def test_change_the_requested_budget_line
    create_budget_line(house, 30, '2015-01')

    form = ChangeBudgetAmountForm.new(
      category: house.id, amount: 14, month: '2015-01')
    ChangeBudgetAmount.new(form).execute

    budget = BudgetRepo.for_month('2015-01')
    assert_equal Money.new(1400), budget[house].amount
  end

  def test_does_not_change_a_newer_budget_line
    create_budget_line(house, 30, '2015-02')

    form = ChangeBudgetAmountForm.new(
      category: house.id, amount: 14, month: '2015-01')
    ChangeBudgetAmount.new(form).execute

    budget = BudgetRepo.for_month('2015-02')
    assert_equal Money.new(3000), budget[house].amount
  end

  def test_does_not_change_another_category
    car = create_category('Car')
    create_budget_line(car, 30, '2015-01')
    create_budget_line(house, 30, '2015-01')

    form = ChangeBudgetAmountForm.new(
      category: house.id, amount: 14, month: '2015-01')
    ChangeBudgetAmount.new(form).execute

    budget = BudgetRepo.for_month('2015-01')
    assert_equal Money.new(3000), budget[car].amount
  end
end
