require 'minitest_helper'

class TestShowBudget < Minitest::Test
  def test_shows_the_budget_for_the_given_month
    create_budget_line(car, 12, '2015-01')
    create_budget_line(car, 30, '2015-02')
    create_budget_line(house, 15, '2015-01')

    form = ShowBudgetForm.new(month: '2015-01')
    result = ShowBudget.new(form).execute

    assert_equal Money.new(1200), result[car].amount
  end

  def test_a_category_without_budget_line_for_the_given_month_is_zero
    create_budget_line(car, 12, '2015-01')

    form = ShowBudgetForm.new(month: '2015-01')
    use_case = ShowBudget.new(form)

    result = use_case.execute

    assert_equal Money.new(0), result[house].amount
  end

  def test_the_total_to_be_budgetted_takes_the_previous_months_into_account
    create_budget_line(car, 10, '2015-01')
    create_budget_line(car, 20, '2015-02')

    form = ShowBudgetForm.new(month: '2015-04')
    use_case = ShowBudget.new(form)

    result = use_case.execute

    assert_budget(Money.new(-3000), [[car, Money.new(0)]], result)
  end
end
