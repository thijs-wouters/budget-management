require 'minitest_helper'

class TestChangeBudgetAmount < Minitest::Test
  def test_amount_must_be_greater_than_or_equal_to_zero
    form = ChangeBudgetAmountForm.new(
      category: house.id, amount: -1, month: '2015-01')
    use_case = ChangeBudgetAmount.new(form)

    refute_validates(
      field: :amount,
      reason: :less_than,
      params: [Money.new(0)]) do
        use_case.execute
      end
  end

  def test_must_be_categorized
    form = ChangeBudgetAmountForm.new(amount: 12, month: '2015-01')
    use_case = ChangeBudgetAmount.new(form)

    refute_validates(field: :category, reason: :not_categorized) do
      use_case.execute
    end
  end

  def test_category_must_exist
    form = ChangeBudgetAmountForm.new(
      category: '1',
      amount: 12,
      month: '2015-01')
    use_case = ChangeBudgetAmount.new(form)

    refute_validates(field: :category, reason: :not_present) do
      use_case.execute
    end
  end

  def test_month_must_be_given
    form = ChangeBudgetAmountForm.new(category: house.id, amount: 12)
    use_case = ChangeBudgetAmount.new(form)

    refute_validates(field: :month, reason: :not_present) do
      use_case.execute
    end
  end

  def test_month_in_wrong_format_raises_exception
    %w(dlkjf sdze-zd 012-01 2015-00 2015-13 2015-1).each do |month|
      use_case = ChangeBudgetAmount.new(
        ChangeBudgetAmountForm.new(
          category: house.id, amount: 12, month: month))

      refute_validates(field: :month, reason: :no_match, params: nil) do
        use_case.execute
      end
    end
  end

  def test_month_has_the_correct_format
    %w(2015-01 2015-12).each do |month|
      form = ChangeBudgetAmountForm.new(
        category: house.id, amount: 12, month: month)
      use_case = ChangeBudgetAmount.new(form)

      use_case.execute
    end
  end
end
