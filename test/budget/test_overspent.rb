require 'minitest_helper'

class TestOverspent < Minitest::Test
  def test_only_add_lines_with_negative_balance
    create_budget_line(house, 200, '2015-03')
    create_budget_line(car, 200, '2015-03')
    create_out_flow(category: house, amount: 300, date: Date.new(2015, 3, 3))

    budget = Budget.for_month('2015-03')
    assert_equal Money.new(100_00), budget.overspent
  end

  def test_adds_a_negative_transaction_to_the_overspent_when_amount_too_high
    create_budget_line(house, 200, '2015-03')
    create_out_flow(category: house, amount: 300, date: Date.new(2015, 3, 3))

    budget = Budget.for_month('2015-03')
    assert_equal Money.new(100_00), budget.overspent
  end

  def test_an_uncategorized_transaction_is_not_added_to_the_overspent
    create_out_flow(category: nil, amount: 300, date: Date.new(2015, 3, 3))

    budget = Budget.for_month('2015-03')
    assert_equal Money.new(0), budget.overspent
  end

  private

  def car
    @car ||= CategoryRepo.create(Category.new(name: 'Car'))
  end
end
