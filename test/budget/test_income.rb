require 'minitest_helper'

class TestIncome < Minitest::Test
  def test_add_positive_transaction_to_the_income
    create_in_flow(category: nil, amount: 200, date: Date.new(2015, 3, 3))

    budget = Budget.for_month('2015-03')
    assert_equal Money.new(200_00), budget.income
  end

  def test_do_not_add_negative_transactions_to_the_income
    create_out_flow(category: nil, amount: 200, date: Date.new(2015, 3, 3))

    budget = Budget.for_month('2015-03')
    assert_equal Money.new(0), budget.income
  end
end
