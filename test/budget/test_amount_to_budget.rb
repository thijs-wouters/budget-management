require 'minitest_helper'

class TestAmountToBudget < Minitest::Test
  def test_add_income_to_the_amount_to_budget
    create_in_flow(category: nil, amount: 200, date: Date.new(2015, 3, 3))

    form = ShowBudgetForm.new(month: '2015-03')
    budget = ShowBudget.new(form).execute

    assert_equal Money.new(200_00), budget.amount_to_budget
  end

  def test_left_over_budget_is_added
    create_in_flow(category: nil, amount: 200, date: Date.new(2015, 3, 3))
    create_budget_line(house, 100, '2015-03')

    form = ShowBudgetForm.new(month: '2015-03')
    budget = ShowBudget.new(form).execute

    assert_equal Money.new(100_00), budget.amount_to_budget
  end

  def test_the_amount_already_budgetted_is_deducted
    create_budget_line(house, 100, '2015-03')

    form = ShowBudgetForm.new(month: '2015-03')
    budget = ShowBudget.new(form).execute

    assert_equal(Money.new(-100_00), budget.amount_to_budget)
  end

  def test_uncategorized_amount_is_deducted
    create_out_flow(category: nil, amount: 200, date: Date.new(2015, 3, 3))

    form = ShowBudgetForm.new(month: '2015-03')
    budget = ShowBudget.new(form).execute

    assert_equal(Money.new(-200_00), budget.amount_to_budget)
  end

  def test_overspent_is_deducted
    create_budget_line(house, 100, '2015-03')
    create_in_flow(category: nil, amount: 100, date: Date.new(2015, 3, 3))
    create_out_flow(category: house, amount: 200, date: Date.new(2015, 3, 3))

    form = ShowBudgetForm.new(month: '2015-03')
    budget = ShowBudget.new(form).execute

    assert_equal(Money.new(-100_00), budget.amount_to_budget)
  end
end
