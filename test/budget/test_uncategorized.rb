require 'minitest_helper'

class TestUncategorized < Minitest::Test
  def test_takes_negative_transactions_without_categories
    create_out_flow(category: nil, amount: 50, date: Date.new(2015, 1, 1))

    form = ShowBudgetForm.new(month: '2015-01')
    budget = ShowBudget.new(form).execute

    assert_equal(Money.new(5000), budget.uncategorized_amount)
  end

  def test_does_not_take_an_income_into_account
    create_in_flow(category: nil, amount: 200, date: Date.new(2015, 1, 1))

    form = ShowBudgetForm.new(month: '2015-01')
    budget = ShowBudget.new(form).execute

    assert_equal(Money.new(0), budget.uncategorized_amount)
  end

  def test_does_not_take_a_categorized_transaction_into_account
    create_out_flow(category: house, amount: 50, date: Date.new(2015, 1, 1))

    form = ShowBudgetForm.new(month: '2015-01')
    budget = ShowBudget.new(form).execute

    assert_equal(Money.new(0), budget.uncategorized_amount)
  end
end
