require 'minitest_helper'
class TestBalance < Minitest::Test
  def test_no_transaction_or_budget_is_zero
    budget = Budget.for_month('2015-03')
    assert_equal Money.new(0), budget[house].balance
  end

  def test_deduct_negative_transaction_from_the_correct_budget_line
    create_budget_line(house, 300, '2015-03')
    create_out_flow(category: house, amount: 200, date: Date.new(2015, 3, 3))

    form = ShowBudgetForm.new(month: '2015-03')
    budget = ShowBudget.new(form).execute

    assert_equal Money.new(100_00), budget[house].balance
  end

  def test_deduct_negative_transaction_from_unbudgetted_category
    create_out_flow(category: house, amount: 200, date: Date.new(2015, 3, 3))

    form = ShowBudgetForm.new(month: '2015-03')
    budget = ShowBudget.new(form).execute

    assert_equal(Money.new(-200_00), budget[house].balance)
  end

  def test_the_balance_is_transfered
    create_budget_line(house, 100, '2015-03')

    form = ShowBudgetForm.new(month: '2015-04')
    budget = ShowBudget.new(form).execute

    assert_equal(Money.new(100_00), budget[house].balance)
  end
end
