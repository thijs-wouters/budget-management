require 'minitest_helper'

class TestHappyPath < Minitest::Test
  def test_can_change_the_date
    form = default_form.tap do |f|
      f.date = Date.new(2015, 1, 2)
    end
    ChangeTransaction.new(form).execute

    budget = Budget.for_month('2015-01')
    assert_equal Date.new(2015, 1, 2), budget.transactions[0].date
  end

  def test_can_change_the_category
    form = default_form.tap do |f|
      f.category = car
    end
    ChangeTransaction.new(form).execute

    budget = Budget.for_month('2015-01')
    assert_equal car, budget.transactions[0].category
  end

  def test_can_change_the_in_flow
    form = default_form.tap do |f|
      f.in_flow = 40
      f.out_flow = 0
    end
    ChangeTransaction.new(form).execute

    budget = Budget.for_month('2015-01')
    assert_equal Money.new(4000), budget.transactions[0].in_flow
  end

  def test_can_change_the_out_flow
    form = default_form.tap do |f|
      f.in_flow = 0
      f.out_flow = 50
    end
    ChangeTransaction.new(form).execute

    budget = Budget.for_month('2015-01')
    assert_equal Money.new(5000), budget.transactions[0].out_flow
  end

  def test_can_change_the_comment
    form = default_form.tap do |f|
      f.comment = 'test'
    end
    ChangeTransaction.new(form).execute

    budget = Budget.for_month('2015-01')
    assert_equal 'test', budget.transactions[0].comment
  end

  def test_category_can_be_nil
    form = default_form.tap do |f|
      f.category = nil
    end
    ChangeTransaction.new(form).execute

    budget = Budget.for_month('2015-01')
    assert_equal NoCategory.new, budget.transactions[0].category
  end

  private

  def default_form
    ChangeTransactionForm.new(
      transaction: create_out_flow(
        category: house,
        amount: 30,
        date: Date.new(2015, 1, 1)),
      category: house,
      date: Date.new(2015, 1, 1),
      in_flow: 0, out_flow: 30,
      comment: nil)
  end
end
