require 'minitest_helper'

class TestValidations < Minitest::Test
  def test_category_must_exist_if_given
    form = default_form.tap do |f|
      f.category = '12'
    end

    refute_validates(field: :category, reason: :not_present) do
      ChangeTransaction.new(form).execute
    end
  end

  def test_transaction_cannot_be_nil
    form = default_form.tap do |f|
      f.transaction = nil
    end

    refute_validates(field: :transaction, reason: :not_present) do
      ChangeTransaction.new(form).execute
    end
  end

  def test_transaction_must_exist
    form = default_form.tap do |f|
      f.transaction = '12'
    end

    refute_validates(field: :transaction, reason: :not_present) do
      ChangeTransaction.new(form).execute
    end
  end

  def test_date_must_be_given
    form = default_form.tap do |f|
      f.date = nil
    end

    refute_validates(field: :date, reason: :not_present) do
      ChangeTransaction.new(form).execute
    end
  end

  def test_one_of_in_or_out_flow_must_be_given
    form = default_form.tap do |f|
      f.in_flow = nil
      f.out_flow = nil
    end

    refute_validates(reason: :neither, params: [:in_flow, :out_flow]) do
      ChangeTransaction.new(form).execute
    end
  end

  def test_one_of_in_or_out_flow_must_be_greater_than_zero
    form = default_form.tap do |f|
      f.in_flow = 0
      f.out_flow = 0
    end

    refute_validates(reason: :neither, params: [:in_flow, :out_flow]) do
      ChangeTransaction.new(form).execute
    end
  end

  def test_not_both_in_and_out_flow_can_be_entered
    form = default_form.tap do |f|
      f.in_flow = 40
      f.out_flow = 50
    end

    refute_validates(reason: :both, params: [:in_flow, :out_flow]) do
      ChangeTransaction.new(form).execute
    end
  end

  private

  def default_form
    ChangeTransactionForm.new(
      transaction: create_out_flow(
        category: house,
        amount: 30,
        date: Date.new(2015, 1, 1)),
      category: house,
      date: Date.new(2015, 1, 1),
      in_flow: 0, out_flow: 30,
      comment: nil)
  end
end
