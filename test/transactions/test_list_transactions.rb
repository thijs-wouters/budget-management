require 'minitest_helper'

class TestListTransactions < Minitest::Test
  def test_list_transactions_starts_with_the_newest
    create_in_flow(date: Date.new(2015, 1, 1))
    create_in_flow(date: Date.new(2015, 1, 3))

    form = ListTransactionsForm.new
    result = ListTransactions.new(form).execute

    assert_equal(Date.new(2015, 1, 3), result[0].date)
  end

  def test_list_transactions_ends_with_the_oldest
    create_in_flow(date: Date.new(2015, 1, 1))
    create_in_flow(date: Date.new(2015, 1, 3))

    form = ListTransactionsForm.new
    result = ListTransactions.new(form).execute

    assert_equal(Date.new(2015, 1, 1), result[1].date)
  end

  def test_returns_the_date
    create_in_flow(date: Date.new(2015, 1, 1))

    form = ListTransactionsForm.new
    result = ListTransactions.new(form).execute

    assert_equal(Date.new(2015, 1, 1), result.first.date)
  end

  def test_returns_the_category
    create_in_flow(category: house)

    form = ListTransactionsForm.new
    result = ListTransactions.new(form).execute

    assert_equal(house, result.first.category)
  end

  def test_returns_out_flow
    create_out_flow(amount: 20)

    form = ListTransactionsForm.new
    result = ListTransactions.new(form).execute

    assert_equal(Money.new(2000), result.first.out_flow)
  end

  def test_returns_in_flow
    create_in_flow(amount: 20)

    form = ListTransactionsForm.new
    result = ListTransactions.new(form).execute

    assert_equal(Money.new(2000), result.first.in_flow)
  end

  def test_returns_the_comment
    create_in_flow(comment: 'comment')

    form = ListTransactionsForm.new
    result = ListTransactions.new(form).execute

    assert_equal('comment', result.first.comment)
  end

  def test_returns_the_budget
    create_in_flow(date: Date.new(2015, 1, 1))

    form = ListTransactionsForm.new
    result = ListTransactions.new(form).execute

    assert_equal(Budget.for_month('2015-01'), result.first.budget)
  end
end
