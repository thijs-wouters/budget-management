require 'minitest_helper'

class TestEffectsOnBudget < Minitest::Test
  def test_adds_an_out_flow_to_the_outflow_of_a_line
    create_budget_line(house, 300, '2015-03')
    create_out_flow(category: house, amount: 200, date: Date.new(2015, 3, 3))

    form = ShowBudgetForm.new(month: '2015-03')
    budget = ShowBudget.new(form).execute

    assert_equal Money.new(200_00), budget[house].outflow
  end
end
