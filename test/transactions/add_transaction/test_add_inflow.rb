require 'minitest_helper'

class TestAddInflow < Minitest::Test
  def test_sets_a_positive_amount
    form = AddTransactionForm.new(
      in_flow: 15,
      date: Date.new(2015, 3, 3))
    AddTransaction.new(form).execute

    budget = Budget.for_month('2015-03')
    assert_equal Money.new(15_00), budget.transactions[0].in_flow
  end

  def test_sets_a_decimal_value
    form = AddTransactionForm.new(
      in_flow: 12.50,
      date: Date.new(2015, 3, 3))
    AddTransaction.new(form).execute

    budget = Budget.for_month('2015-03')
    assert_equal Money.new(12_50), budget.transactions[0].in_flow
  end

  def test_sets_the_date
    form = AddTransactionForm.new(
      in_flow: 15,
      date: Date.new(2015, 3, 3))
    AddTransaction.new(form).execute

    budget = Budget.for_month('2015-03')
    assert_equal Date.new(2015, 3, 3), budget.transactions[0].date
  end

  def test_sets_the_comment
    form = AddTransactionForm.new(
      in_flow: 15,
      date: Date.new(2015, 3, 3),
      comment: 'comment')
    AddTransaction.new(form).execute

    budget = Budget.for_month('2015-03')
    assert_equal 'comment', budget.transactions[0].comment
  end

  def test_sets_the_category
    form = AddTransactionForm.new(
      category: house.id,
      in_flow: 15,
      date: Date.new(2015, 3, 3),
      comment: 'comment')
    AddTransaction.new(form).execute

    budget = Budget.for_month('2015-03')
    assert_equal house, budget.transactions[0].category
  end

  def test_category_must_exist_if_given
    form = AddTransactionForm.new(
      category: '12',
      in_flow: 15,
      date: Date.new(2015, 3, 3))
    use_case = AddTransaction.new(form)

    refute_validates(field: :category, reason: :not_present) do
      use_case.execute
    end
  end

  def test_date_must_be_given
    form = AddTransactionForm.new(
      in_flow: 15)
    use_case = AddTransaction.new(form)

    refute_validates(field: :date, reason: :not_present) do
      use_case.execute
    end
  end

  def test_inflow_must_be_greater_than_zero
    form = AddTransactionForm.new(
      in_flow: 0,
      date: Date.new(2015, 3, 3))
    use_case = AddTransaction.new(form)

    refute_validates(reason: :neither, params: [:in_flow, :out_flow]) do
      use_case.execute
    end
  end

  def test_outflow_should_not_be_present
    form = AddTransactionForm.new(
      out_flow: 12,
      in_flow: 12,
      date: Date.new(2015, 3, 3))
    use_case = AddTransaction.new(form)

    refute_validates reason: :both, params: [:in_flow, :out_flow] do
      use_case.execute
    end
  end

  def test_inflow_should_be_filled_in
    form = AddTransactionForm.new(
      date: Date.new(2015, 3, 3))
    use_case = AddTransaction.new(form)

    refute_validates reason: :neither, params: [:in_flow, :out_flow] do
      use_case.execute
    end
  end
end
