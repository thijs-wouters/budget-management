require 'minitest_helper'

class TestAddOutflow < Minitest::Test
  def test_sets_a_negative_amount
    form = AddTransactionForm.new(
      out_flow: 15,
      date: Date.new(2015, 3, 3))
    AddTransaction.new(form).execute

    budget = Budget.for_month('2015-03')
    assert_equal Money.new(1500), budget.transactions[0].out_flow
  end

  def test_sets_the_date
    form = AddTransactionForm.new(
      out_flow: 15,
      date: Date.new(2015, 3, 3))
    AddTransaction.new(form).execute

    budget = Budget.for_month('2015-03')
    assert_equal Date.new(2015, 3, 3), budget.transactions[0].date
  end

  def test_sets_the_comment
    form = AddTransactionForm.new(
      out_flow: 15,
      date: Date.new(2015, 3, 3),
      comment: 'comment')
    AddTransaction.new(form).execute

    budget = Budget.for_month('2015-03')
    assert_equal 'comment', budget.transactions[0].comment
  end

  def test_sets_the_category
    form = AddTransactionForm.new(
      category: house.id,
      out_flow: 15,
      date: Date.new(2015, 3, 3),
      comment: 'comment')
    AddTransaction.new(form).execute

    budget = Budget.for_month('2015-03')
    assert_equal house, budget.transactions[0].category
  end

  def test_outflow_must_be_greater_than_zero
    form = AddTransactionForm.new(
      out_flow: 0,
      date: Date.new(2015, 3, 3))
    use_case = AddTransaction.new(form)

    refute_validates(reason: :neither, params: [:in_flow, :out_flow]) do
      use_case.execute
    end
  end

  def test_inflow_should_not_be_present
    form = AddTransactionForm.new(
      out_flow: 12,
      in_flow: 12,
      date: Date.new(2015, 3, 3))
    use_case = AddTransaction.new(form)

    refute_validates reason: :both, params: [:in_flow, :out_flow] do
      use_case.execute
    end
  end

  def test_inflow_can_be_empty
    form = AddTransactionForm.new(
      out_flow: 12,
      in_flow: '',
      date: Date.new(2015, 3, 3))
    AddTransaction.new(form).execute

    budget = Budget.for_month('2015-03')
    assert_equal Money.new(1200), budget.transactions[0].out_flow
  end

  def test_outflow_should_be_filled_in
    form = AddTransactionForm.new(date: Date.new(2015, 3, 3))
    use_case = AddTransaction.new(form)

    refute_validates reason: :neither, params: [:in_flow, :out_flow] do
      use_case.execute
    end
  end
end
