require 'tilt/erubis'

require 'web/budget_page'
require 'web/transactions_page'

set :erb, escape_html: true

get '/budget/:month' do
  form = ShowBudgetForm.new(month: params['month'])
  @page = BudgetPage.new(budget: ShowBudget.new(form).execute)
  erb :budget
end

post '/categories' do
  begin
    form = AddCategoryForm.new(name: params['name'])
    AddCategory.new(form).execute
    redirect "/budget/#{params['month']}"
  rescue ValidationError => e
    show_budget_form = ShowBudgetForm.new(month: params['month'])
    @page = BudgetPage.new(
      budget: ShowBudget.new(show_budget_form).execute,
      errors: e.errors)
    erb :budget
  end
end

put '/budget/:month/:category_id' do
  begin
    form = ChangeBudgetAmountForm.new(
      category: params['category_id'],
      month: params['month'],
      amount: params['amount'])
    ChangeBudgetAmount.new(form).execute
    redirect "/budget/#{params['month']}"
  rescue ValidationError => e
    form = ShowBudgetForm.new(month: params['month'])
    @page = BudgetPage.new(
      budget: ShowBudget.new(form).execute,
      errors: e.errors)
    erb :budget
  end
end

get '/transactions' do
  form = ListTransactionsForm.new
  @page = TransactionsPage.new(transactions: ListTransactions.new(form).execute)
  erb :transactions
end

post '/transactions' do
  begin
    form = AddTransactionForm.new(
      date: params['date'],
      in_flow: params['in_flow'],
      out_flow: params['out_flow'],
      category: params['category'],
      comment: params['comment'])
    AddTransaction.new(form).execute
    redirect '/transactions'
  rescue ValidationError => e
    form = ListTransactionsForm.new
    @page = TransactionsPage.new(
      transactions: ListTransactions.new(form).execute,
      errors: e.errors)
    erb :transactions
  end
end

put '/transactions/:id' do
  begin
    form = ChangeTransactionForm.new(
      transaction: params['id'],
      date: params['date'],
      in_flow: params['in_flow'],
      out_flow: params['out_flow'],
      category: params['category'],
      comment: params['comment'])
    ChangeTransaction.new(form).execute
    redirect '/transactions'
  rescue ValidationError => e
    form = ListTransactionsForm.new
    @page = TransactionsPage.new(
      transactions: ListTransactions.new(form).execute,
      errors: e.errors)
    erb :transactions
  end
end
