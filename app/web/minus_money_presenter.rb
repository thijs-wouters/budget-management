require_relative 'money_presenter'

class MinusMoneyPresenter < MoneyPresenter
  def initialize(value)
    @value = value
    super
  end

  def css_class
    if @value > Money.new(0)
      'list-group-item-danger'
    else
      'list-group-item-success'
    end
  end
end
