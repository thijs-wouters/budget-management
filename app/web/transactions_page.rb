require_relative 'transaction_presenter'
require_relative 'category_presenter'
require_relative 'error_presenter'

class TransactionsPage
  def initialize(transactions:, errors: [])
    @transactions = transactions
    @errors = errors
  end

  def errors
    @errors.map { |error| ErrorPresenter.new(error) }
  end

  def transactions
    @transactions.map do |transaction|
      TransactionPresenter.new(transaction, self)
    end
  end

  def possible_categories(selected_category = NoCategory.new)
    [CategoryPresenter.new(NoCategory.new, selected_category)].push(
      *CategoryRepo.all.map do |category|
        CategoryPresenter.new(category, selected_category)
      end)
  end
end
