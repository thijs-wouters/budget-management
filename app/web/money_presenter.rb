class MoneyPresenter
  def initialize(value)
    @value = value
  end

  def to_s
    format('%.2f', @value)
  end
end
