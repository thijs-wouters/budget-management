class BudgetLinePresenter
  def initialize(budget_line)
    @budget_line = budget_line
  end

  def id
    @budget_line.category.id
  end

  def category
    @budget_line.category.name
  end

  def amount
    MoneyPresenter.new(@budget_line.amount)
  end

  def balance
    MoneyPresenter.new(@budget_line.balance)
  end

  def outflow
    MoneyPresenter.new(@budget_line.outflow)
  end

  def input_field_id
    "category_#{@budget_line.category.id}"
  end

  def change_budget_button_id
    "change_category_#{@budget_line.category.id}"
  end

  def change_amount_url
    "/budget/#{@budget_line.budget.month}/#{@budget_line.category.id}"
  end

  def css_class
    'danger' if @budget_line.balance < Money.new(0)
  end
end
