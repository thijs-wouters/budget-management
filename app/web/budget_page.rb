require_relative 'budget_line_presenter'
require_relative 'plus_money_presenter'
require_relative 'minus_money_presenter'
require_relative 'title_decorator'

class BudgetPage
  def initialize(budget:, errors: [])
    @budget = budget
    @errors = errors
  end

  def errors
    @errors.map { |error| ErrorPresenter.new(error) }
  end

  def lines
    @budget.categories.map do |category|
      BudgetLinePresenter.new(@budget[category])
    end
  end

  def selected_month
    @budget.month
  end

  def amount_to_budget
    TitleDecorator.new(
      'Amount to budget',
      PlusMoneyPresenter.new(@budget.amount_to_budget))
  end

  def overspent
    TitleDecorator.new(
      'Overspent',
      MinusMoneyPresenter.new(@budget.overspent))
  end

  def uncategorized_amount
    TitleDecorator.new(
      'Uncategorized amount',
      MinusMoneyPresenter.new(@budget.uncategorized_amount))
  end

  def remaining_last_month
    TitleDecorator.new(
      'Remaining last month',
      PlusMoneyPresenter.new(@budget.remaining_last_month))
  end

  def income
    TitleDecorator.new(
      'Income',
      PlusMoneyPresenter.new(@budget.income))
  end
end
