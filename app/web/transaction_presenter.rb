class TransactionPresenter
  def initialize(transaction, page)
    @transaction = transaction
    @page = page
  end

  def id
    @transaction.id
  end

  def date
    @transaction.date.strftime('%d-%m-%Y')
  end

  def in_flow
    MoneyPresenter.new(@transaction.in_flow)
  end

  def out_flow
    MoneyPresenter.new(@transaction.out_flow)
  end

  def comment
    @transaction.comment
  end

  def row_class
    'warning' unless @transaction.category.categorized?
  end

  def possible_categories
    @page.possible_categories(@transaction.category)
  end

  def change_url
    "/transactions/#{@transaction.id}"
  end
end
