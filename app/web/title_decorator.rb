class TitleDecorator < SimpleDelegator
  attr_reader :title

  def initialize(title, value)
    @title = title
    super(value)
  end

  def id
    title.tr(' ', '_').downcase
  end
end
