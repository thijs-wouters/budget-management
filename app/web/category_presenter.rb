class CategoryPresenter
  def initialize(category, selected_category)
    @category = category
    @selected_category = selected_category
  end

  def id
    @category.id
  end

  def name
    @category.name
  end

  def selected
    'selected' if @category.id == @selected_category.id
  end
end
