class ErrorPresenter
  REASONS = {
    not_present: 'must be present',
    less_than: 'must be larger than 0',
    neither: 'Either inflow or outflow must be larger than 0',
    both: 'Not both inflow and outflow can be larger than 0'
  }

  def initialize(error)
    @field = error.field if error.respond_to? :field
    @reason = error.reason
  end

  def message
    "#{field} #{reason}"
  end

  def field
    @field.to_s.capitalize
  end

  def reason
    REASONS[@reason]
  end
end
