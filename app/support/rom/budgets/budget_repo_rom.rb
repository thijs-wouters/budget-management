class BudgetRepoROM
  attr_reader :rom

  def initialize(rom)
    @rom = rom
  end

  def for_month(month)
    rom.relation(:budgets)
      .combine(BudgetLineRepoROM.new(rom).for_budget)
      .combine(TransactionRepoROM.new(rom).for_budget)
      .for_month(month)
      .as(:budget)
      .one
  end

  def remaining?(month)
    !rom.relation(:budgets)
      .before(month)
      .to_a.empty?
  end

  def create(budget)
    Budget.new(rom.command(:budgets).create.call(month: budget.month).first)
  end

  def empty?
    rom.relation(:budgets).to_a.empty?
  end

  def clear
    rom.command(:budgets).delete.call
  end
end
