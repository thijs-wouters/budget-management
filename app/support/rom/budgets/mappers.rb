require_relative '../../../budget/entity/budget'
require_relative '../../../budget/entity/budget_line'
require_relative '../../../transactions/entity/transaction'
require_relative '../../../categories/entity/category'

ROM.mappers do
  define(:budgets) do
    register_as :budget
    model Budget

    combine :lines, on: { id: :budget_id } do
      model BudgetLine

      combine :category, on: { category_id: :id }, type: :hash do
        model Category
      end

      combine :budget, on: { budget_id: :id }, type: :hash do
        model Budget

        combine :transactions, on: { id: :budget_id } do
          model Transaction

          combine :category, on: { category_id: :id }, type: :hash do
            model Category
          end

          combine :budget, on: { budget_id: :id }, type: :hash do
            model Budget
          end
        end
      end
    end

    combine :transactions, on: { id: :budget_id } do
      model Transaction

      combine :category, on: { category_id: :id }, type: :hash do
        model Category
      end

      combine :budget, on: { budget_id: :id }, type: :hash do
        model Budget
      end
    end
  end
end
