ROM.relation(:budgets) do
  def for_relation(relation)
    restrict(id: relation.map { |rel| rel[:budget_id] })
  end

  def for_month(month)
    restrict(month: month)
  end

  def before(selected_month)
    order(:month).restrict do |tuple|
      tuple[:month] < selected_month
    end
  end
end
