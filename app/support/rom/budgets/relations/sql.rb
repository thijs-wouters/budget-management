ROM.relation(:budgets) do
  def for_relation(relation)
    where(id: relation.map { |rel| rel[:budget_id] })
  end

  def for_month(month)
    where(month: month)
  end

  def before(selected_month)
    order(:month).where do
      month < selected_month
    end
  end
end
