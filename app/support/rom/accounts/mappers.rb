require 'accounts/entity/account'

ROM.mappers do
  define(:accounts) do
    register_as :account
    model Account
  end
end
