class TransactionRepoROM
  attr_reader :rom

  def initialize(rom)
    @rom = rom
  end

  def all
    rom.relation(:transactions)
      .order_by(:date)
      .combine(rom.relation(:categories).for_relation)
      .combine(rom.relation(:budgets).for_relation)
      .as(:transaction).to_a.reverse
  end

  def find(id)
    rom.relation(:transactions)
      .by_id(id)
      .combine(rom.relation(:categories).for_relation)
      .combine(rom.relation(:budgets).for_relation)
      .as(:transaction).one
  end

  def for_budget
    rom.relation(:transactions)
      .combine(rom.relation(:categories).for_relation)
      .combine(rom.relation(:budgets).for_relation)
      .for_budgets
  end

  def empty?
    rom.relation(:transactions).to_a.empty?
  end

  def create(transaction)
    map(transaction, rom.command(:transactions).create.call(
      ROMTransaction.new(transaction)
    ).first)
  end

  def update(transaction)
    map(transaction, rom.command(:transactions).update.call(
      ROMTransaction.new(transaction)
    ).first)
  end

  def clear
    rom.command(:transactions).delete.call
  end

  class ROMTransaction
    def initialize(transaction)
      @transaction = transaction
    end

    def to_hash
      {
        category_id: category_id,
        in_flow: in_flow,
        out_flow: out_flow,
        date: @transaction.date,
        comment: @transaction.comment,
        budget_id: budget_id
      }
    end

    private

    def category_id
      @transaction.category.id
    end

    def in_flow
      @transaction.in_flow.cents
    end

    def out_flow
      @transaction.out_flow.cents
    end

    def budget_id
      @transaction.budget.id
    end
  end

  def map(transaction, hash)
    Transaction.new(
      hash.merge(
        budget: transaction.budget,
        category: transaction.category))
  end
end
