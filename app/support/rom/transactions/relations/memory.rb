ROM.relation(:transactions) do
  def order_by(field)
    order(field)
  end

  def for_budgets(budgets)
    restrict(budget_id: budgets.map { |budget| budget[:id] })
  end

  def by_id(id)
    restrict(id: id)
  end
end
