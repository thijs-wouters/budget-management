require_relative '../../../budget/entity/budget'
require_relative '../../../transactions/entity/transaction'
require_relative '../../../categories/entity/category'

ROM.mappers do
  define(:transactions) do
    register_as :transaction

    step do
      combine :categories, on: { category_id: :id } do
        model Category
      end

      combine :budget, on: { budget_id: :id }, type: :hash do
        model Budget
      end
    end

    step do
      unfold :category, from: :categories
    end

    step do
      model Transaction
    end
  end
end
