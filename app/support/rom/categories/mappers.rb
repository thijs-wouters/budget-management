require_relative '../../../categories/entity/category'

ROM.mappers do
  define(:categories) do
    register_as :category
    model Category
  end
end
