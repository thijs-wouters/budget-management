ROM.relation(:categories) do
  def find(id)
    where(id: id)
  end

  def for_relation(relation)
    where(id: relation.map { |rel| rel[:category_id] })
  end
end
