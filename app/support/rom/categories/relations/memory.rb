ROM.relation(:categories) do
  def find(id)
    restrict(id: id)
  end

  def for_relation(relation)
    restrict(id: relation.map { |rel| rel[:category_id] })
  end
end
