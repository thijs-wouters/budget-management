class CategoryRepoROM
  attr_reader :rom

  def initialize(rom)
    @rom = rom
  end

  def create(category)
    Category.new(
      rom.command(:categories).create.call(
        name: category.name).first)
  end

  def empty?
    rom.relation(:categories).to_a.empty?
  end

  def clear
    rom.command(:categories).delete.call
  end

  def count
    rom.relation(:categories).to_a.size
  end

  def first
    rom.relation(:categories).as(:category).first
  end

  def find(id)
    rom.relation(:categories).find(id).as(:category).one
  end

  def all
    rom.relation(:categories).as(:category).to_a
  end
end
