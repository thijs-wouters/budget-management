ROM.relation(:budget_lines) do
  def for_budgets(budgets)
    restrict(budget_id: budgets.map { |budget| budget[:id] })
  end

  def find(id)
    restrict(id: id)
  end
end
