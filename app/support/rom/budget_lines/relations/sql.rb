ROM.relation(:budget_lines) do
  def for_budgets(budgets)
    where(budget_id: budgets.map { |budget| budget[:id] })
  end

  def find(id)
    where(id: id)
  end
end
