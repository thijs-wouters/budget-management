class BudgetLineRepoROM
  attr_reader :rom

  def initialize(rom)
    @rom = rom
  end

  def create(budget_line)
    BudgetLine.new(
      rom.command(:budget_lines).create.call(
        budget_id: budget_line.budget.id,
        category_id: budget_line.category.id,
        amount: budget_line.amount.cents
      ).first
      .merge(category: budget_line.category))
  end

  def for_budget
    rom.relation(:budget_lines)
      .combine(rom.relation(:categories).for_relation)
      .combine(rom.relation(:budgets)
                .combine(TransactionRepoROM.new(rom).for_budget)
                .for_relation)
      .for_budgets
  end

  def update(budget_line)
    rom.command(:budget_lines)
      .update.find(budget_line.id).call(amount: budget_line.amount.cents)
  end

  def empty?
    rom.relation(:budget_lines).to_a.empty?
  end

  def clear
    rom.command(:budget_lines).delete.call
  end
end
