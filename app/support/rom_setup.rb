IMPL = 'memory'

require 'rom'
require "rom/#{IMPL}"
require 'rom/generated_id'

ROM.use :auto_registration

require_relative "rom/setup/#{IMPL}"

require_relative "rom/categories/relations/#{IMPL}"
require_relative 'rom/categories/commands'
require_relative 'rom/categories/mappers'
require_relative "rom/budget_lines/relations/#{IMPL}"
require_relative 'rom/budget_lines/commands'
require_relative "rom/transactions/relations/#{IMPL}"
require_relative 'rom/transactions/commands'
require_relative 'rom/transactions/mappers'
require_relative "rom/budgets/relations/#{IMPL}"
require_relative 'rom/budgets/commands'
require_relative 'rom/budgets/mappers'
require_relative "rom/accounts/relations/#{IMPL}"
require_relative 'rom/accounts/commands'
require_relative 'rom/accounts/mappers'

ROM.finalize
