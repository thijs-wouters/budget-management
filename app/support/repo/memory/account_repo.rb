require 'accounts/repo/account_repo'

module Memory
  class AccountRepo
    def initialize
      @accounts = []
    end

    def clear
      @accounts.clear
    end

    def empty?
      @accounts.empty?
    end

    def create(account)
      @accounts << account
      account
    end

    def all
      @accounts
    end
  end
end

AccountRepo.register :memory, Memory::AccountRepo.new
