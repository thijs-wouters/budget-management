require 'categories/repo/category_repo'
require 'categories/entity/category'

module Memory
  class CategoryRepo
    def initialize
      @categories = []
    end

    def clear
      @categories.clear
    end

    def empty?
      @categories.empty?
    end

    def all
      @categories
    end

    def create(category)
      @categories << category
      category
    end

    def find(id)
      @categories.find do |category|
        category.id == id
      end
    end
  end
end

CategoryRepo.register :memory, Memory::CategoryRepo.new
