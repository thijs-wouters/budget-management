class Form
  include Sanatio
  include Virtus.model

  def validate!
    fail ValidationError, errors unless valid?
  end

  class Money < Virtus::Attribute
    def coerce(value)
      if value.nil? || value.to_s.empty?
        ::Money.new(0)
      else
        ::Money.new(value.to_f * 100)
      end
    end
  end

  class Transaction < Virtus::Attribute
    def coerce(value)
      return value if value.instance_of? ::Transaction

      TransactionRepo.find(value)
    end
  end

  class Category < Virtus::Attribute
    def coerce(value)
      return value if value.instance_of? ::Category

      if value.nil? || value.to_s.empty?
        ::NoCategory.new
      else
        CategoryRepo.find(value)
      end
    end
  end
end
