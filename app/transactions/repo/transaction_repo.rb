require 'interchange'

class TransactionRepo
  extend Interchange.new(
    :create,
    :update,
    :all,
    :find,
    :empty?,
    :clear)
end
