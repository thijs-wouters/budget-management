class ChangeTransaction
  attr_accessor :form

  def initialize(form)
    @form = form
  end

  def execute
    form.validate!

    form.transaction.update(
      date: form.date,
      category: form.category,
      in_flow: form.in_flow,
      out_flow: form.out_flow,
      comment: form.comment)
  end
end
