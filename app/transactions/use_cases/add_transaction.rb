class AddTransaction
  attr_reader :form

  def initialize(form)
    @form = form
  end

  def execute
    form.validate!

    Transaction.new(
      category: form.category,
      budget: budget,
      in_flow: form.in_flow,
      out_flow: form.out_flow,
      date: form.date,
      comment: form.comment
    ).save
  end

  private

  def budget
    Budget.for_month(form.date.strftime('%Y-%m')).save
  end
end
