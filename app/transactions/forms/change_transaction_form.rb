class ChangeTransactionForm < Form
  ensure_that(self).is Either, :in_flow, :out_flow, GreaterThan, ::Money.new(0)

  attribute :transaction, Transaction
  ensure_that(:transaction).is Present

  attribute :date, Date
  ensure_that(:date).is Present

  attribute :category, Category
  ensure_that(:category).is Present

  attribute :in_flow, Money
  attribute :out_flow, Money
  attribute :comment, String
end
