class AddTransactionForm < Form
  ensure_that(self).is Either, :in_flow, :out_flow, GreaterThan, ::Money.new(0)

  attribute :date, Date
  ensure_that(:date).is Present

  attribute :category, Category
  ensure_that(:category).is Present

  attribute :in_flow, Money, nullify_blank: true
  attribute :out_flow, Money, nullify_blank: true

  attribute :comment, String
end
