class Transaction
  attr_reader :id, :budget, :in_flow, :out_flow, :date, :category, :comment

  def initialize(attributes)
    @id = attributes[:id]
    @category = attributes[:category] || NoCategory.new
    @in_flow = Money.new(attributes[:in_flow])
    @out_flow = Money.new(attributes[:out_flow])
    @date = attributes[:date]
    @comment = attributes[:comment]
    @budget = attributes[:budget]
  end

  def update(attributes)
    @date = attributes[:date]
    @category = attributes[:category]
    @in_flow = attributes[:in_flow]
    @out_flow = attributes[:out_flow]
    @comment = attributes[:comment]
    save
  end

  def save
    if id
      TransactionRepo.update(self)
    else
      return self if id
      budget[category].save if category.categorized?
      TransactionRepo.create(self)
    end
  end

  def categorized?
    category.categorized?
  end
end
