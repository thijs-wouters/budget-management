class NoCategory
  attr_reader :id

  def categorized?
    false
  end

  def ==(other)
    other.instance_of?(NoCategory)
  end

  def name
    'Uncategorized'
  end
end
