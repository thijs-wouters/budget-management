class Category
  attr_reader :id, :name

  def initialize(attributes)
    @id = attributes[:id]
    @name = attributes[:name]
  end

  def categorized?
    true
  end

  def ==(other)
    id == other.id
  end

  def save
    CategoryRepo.create(self)
  end
end
