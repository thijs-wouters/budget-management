require 'interchange'

class CategoryRepo
  extend Interchange.new(
    :create,
    :empty?,
    :clear,
    :count,
    :first,
    :find,
    :all)
end
