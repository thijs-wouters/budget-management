class AddCategory
  attr_reader :form

  def initialize(form)
    @form = form
  end

  def execute
    form.validate!

    Category.new(name: form.name).save
  end
end
