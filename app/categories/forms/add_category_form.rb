class AddCategoryForm < Form
  attribute :name, String
  ensure_that(:name).is Present
end
