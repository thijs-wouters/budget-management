class Account
  attr_reader :id, :name

  def initialize(attributes)
    @id = attributes[:id]
    @name = attributes[:name]
  end

  def save
    AccountRepo.create(self)
  end
end
