require 'interchange'

class AccountRepo
  extend Interchange.new(
    :create,
    :all,
    :clear,
    :empty?)
end
