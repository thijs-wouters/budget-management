class BudgetLine
  attr_reader :id, :category, :budget
  attr_accessor :amount
  attr_writer :budget

  def initialize(attributes)
    @id = attributes[:id]
    @budget = attributes[:budget]
    @category = attributes[:category]
    @amount = Money.new(attributes[:amount] || 0)
  end

  def save
    if id
      BudgetLineRepo.update(self)
    else
      BudgetLineRepo.create(self)
    end
  end

  def balance
    budget.previous_balance(category) + amount - outflow
  end

  def outflow
    budget.transactions_for(category).map(&:out_flow).reduce(0, :+)
  end
end
