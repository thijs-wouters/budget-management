class Budget
  attr_reader :id, :month, :transactions

  def self.for_month(month)
    BudgetRepo.for_month(month) || Budget.new(month: month)
  end

  def initialize(attributes)
    @id = attributes[:id]
    @month = attributes[:month]
    @lines = attributes[:lines] || []
    @transactions = attributes[:transactions] || []
  end

  def change(category, amount)
    self[category].amount = amount
    self
  end

  def save
    @id = BudgetRepo.create(self).id unless id
    @lines.each(&:save)
    self
  end

  def ==(other)
    other.id == id if other
  end

  def amount_to_budget
    remaining_last_month +
      income -
      overspent -
      amount_budgetted -
      uncategorized_amount
  end

  def amount_budgetted
    @lines.map(&:amount).reduce(Money.new(0), :+)
  end

  def income
    @transactions
      .map(&:in_flow)
      .reduce(Money.new(0), :+)
  end

  def overspent
    - @lines
      .map(&:balance)
      .select { |balance| balance < Money.new(0) }
      .reduce(Money.new(0), :+)
  end

  def uncategorized_amount
    @transactions
      .select { |transaction| !transaction.categorized? }
      .map(&:out_flow).reduce(Money.new(0), :+)
  end

  def remaining_last_month
    BudgetRepo.remaining?(month) ? previous.amount_to_budget : Money.new(0)
  end

  def categories
    CategoryRepo.all
  end

  def [](category)
    @lines.find { |line| line.category.id == category.id } || add_line(category)
  end

  def previous
    Budget.for_month(previous_month)
  end

  def transactions_for(category)
    @transactions.select { |transaction| transaction.category == category }
  end

  def previous_balance(category)
    BudgetRepo.remaining?(month) ? previous[category].balance : Money.new(0)
  end

  private

  def previous_month
    Date.strptime(month, '%Y-%m').prev_month.strftime('%Y-%m')
  end

  def add_line(category)
    BudgetLine.new(
      budget: self,
      category: category).tap do |new_line|
        @lines << new_line
      end
  end
end
