class ChangeBudgetAmount
  attr_reader :form

  def initialize(form)
    @form = form
  end

  def execute
    form.validate!

    budget = Budget.for_month(form.month)
    budget.change(form.category, form.amount)
    budget.save
  end
end
