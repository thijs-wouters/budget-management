class ShowBudget
  attr_reader :form

  def initialize(form)
    @form = form
  end

  def execute
    Budget.for_month(form.month)
  end
end
