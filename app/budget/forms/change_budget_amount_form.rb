class ChangeBudgetAmountForm < Form
  attribute :category, Category
  ensure_that(:category)
    .is { categorized? }
    .skip_if { nil? }.reason = :not_categorized
  ensure_that(:category).is Present

  attribute :month, String
  ensure_that(:month).is Present
  ensure_that(:month).is Matching, /^\d{4}\-(0[1-9]|1[0-2])$/

  attribute :amount, Money
  ensure_that(:amount).is Present
  ensure_that(:amount).is GreaterThanOrEqualTo, ::Money.new(0)
end
