require 'interchange'

class BudgetLineRepo
  extend Interchange.new(
    :create,
    :for_budget,
    :update,
    :clear,
    :empty?)
end
