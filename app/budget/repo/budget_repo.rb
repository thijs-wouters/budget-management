require 'interchange'

class BudgetRepo
  extend Interchange.new(
    :for_month,
    :remaining?,
    :create,
    :empty?,
    :clear)
end
